const n                 = 2;
if(n > 0){
    const matrixSize        = 2*n-1;
    const directions        = ["left", "bottom", "right", "top"];
    function buildMatrix(size){
        var array   = [];
        var value   = 1;
        for(var i = 0; i < size; i++){
            array.push([]);
            for(var j = 0; j < size; j++){
                array[i].push(value);
                value++;
            }
        }
        return array;
    }

    var matrix = buildMatrix(matrixSize);
    extractValues(matrix);

    function extractValues(matrix){

        var row                 = n-1;
        var col                 = n-1;
        var stepSize            = 1;
        var stepsCount          = 0;
        var direction           = directions[0];

        var result              = matrix[row][col];

        function turn(){
            var directionIndex = directions.indexOf(direction);
            direction = directionIndex < directions.length - 1 ? directions[directionIndex + 1] : directions[0];
        }

        function processExtracting(){
            for(var i = 0; i < stepSize; i++){
                switch(direction){
                    case "left":
                        col--;
                        break;
                    case "bottom":
                        row++;
                        break;
                    case "right":
                        col++;
                        break;
                    case "top":
                        row--;
                        break;
                }
                result += " " + matrix[row][col];
            }
            turn();
            stepsCount++;
        }

        while(stepSize < matrixSize){
            processExtracting();
            if(stepsCount == 2){
                if(stepSize == matrixSize-1){
                    processExtracting();
                    break;
                }
                stepsCount = 0;
                stepSize++;
            }
        }
        console.log(result);
    }
}
